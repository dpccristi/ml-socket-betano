const fs = require('fs');
learning();

async function learning() {
    const games = JSON.parse(fs.readFileSync('betanolive.json'));

    options = {
        final_results1: 1,
        final_results2: 0,
        final_resultsX: 0,
        over0_5: 0,
        under0_5: 0,
        over1_5: 0,
        under1_5: 0,
        over2_5: 0,
        under2_5: 0,
        over3_5: 0,
        under3_5: 0,
        over4_5: 0,
        under4_5: 0
    };
     console.log(set_bet(options));
}

function set_bet(options) {
    const gamesold = JSON.parse(fs.readFileSync('start_bet.json'));
    var saved = [];
    for (var property in options) {
        if (options[property] == 1) {
            saved.push(property)
        }
    }
    return saved;
}

function final_results1(game) {
    score = game['score'];
    score = score.replace("-", "");
    home = score[0];
    away = score[1];
    if (home > away) {
        return 1;
    }
    return 0;
}

function final_results2(game) {
    score = game['score'];
    score = score.replace("-", "");
    home = score[0];
    away = score[1];
    if (home < away) {
        return 1;
    }
    return 0;
}

function final_resultsX(game) {
    score = game['score'];
    score = score.replace("-", "");
    home = score[0];
    away = score[1];
    if (home == away) {
        return 1;
    }
    return 0;
}

function over0_5(game) {
    score = game['score'];
    score = score.replace("-", "");
    home = score[0];
    away = score[1];
    return (home + away) >= 1;
}

function under0_5(game) {
    score = game['score'];
    score = score.replace("-", "");
    home = score[0];
    away = score[1];
    return (home + away) === 0;
}

function over1_5(game) {
    score = game['score'];
    score = score.replace("-", "");
    home = score[0];
    away = score[1];
    return (home + away) >= 2;
}

function under1_5(game) {
    score = game['score'];
    score = score.replace("-", "");
    home = score[0];
    away = score[1];
    return (home + away) <= 1;
}

function over2_5(game) {
    score = game['score'];
    score = score.replace("-", "");
    home = score[0];
    away = score[1];
    return (home + away) >= 3;
}

function under2_5(game) {
    score = game['score'];
    score = score.replace("-", "");
    home = score[0];
    away = score[1];
    return (home + away) <= 2;
}

function over3_5(game) {
    score = game['score'];
    score = score.replace("-", "");
    home = score[0];
    away = score[1];
    return (home + away) >= 4;
}

function under3_5(game) {
    score = game['score'];
    score = score.replace("-", "");
    home = score[0];
    away = score[1];
    return (home + away) <= 3;
}

function over4_5(game) {
    score = game['score'];
    score = score.replace("-", "");
    home = score[0];
    away = score[1];
    return (home + away) >= 5;
}

function under4_5(game) {
    score = game['score'];
    score = score.replace("-", "");
    home = score[0];
    away = score[1];
    return (home + away) <= 4;
}

function next_goal(game) {

}