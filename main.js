const fs = require('fs');
const puppeteer = require('puppeteer');

function sleep(time) {
    return new Promise((resolve) => setTimeout(resolve, time));
}

function wait(ms) {
    return new Promise(resolve => setTimeout(() => resolve(), ms));
}

async function click(page, el) {
    const allResultsSelector = el;
    await page.waitForSelector(allResultsSelector);
    await page.click(allResultsSelector);
    await page.waitForSelector(allResultsSelector);
    return page;
}

go();

async function go() {

    const browser = await puppeteer.launch({
        headless: false,
        args: ['--no-sandbox']
    });
    let page = await browser.newPage();
    await page.setViewport({width: 1366, height: 1000});
    let url = 'https://ro.betano.com/live/#FOOT';
    await page.goto(url, {waitUntil: 'load'});
    const bodyHandle = await page.$('body');
    const {height} = await bodyHandle.boundingBox();
    await bodyHandle.dispose();
    const viewportHeight = page.viewport().height;
    let viewportIncr = 0;
    while (viewportIncr + viewportHeight < height) {
        await page.evaluate(_viewportHeight => {
            window.scrollBy(0, _viewportHeight);
        }, viewportHeight);
        await wait(20);
        viewportIncr = viewportIncr + viewportHeight;
    }
    await page.evaluate(_ => {
        window.scrollTo(0, 0);
    });
    await wait(200);
    await page.addScriptTag({url: 'https://code.jquery.com/jquery-3.2.1.min.js'});
    await(100);
    await page.addScriptTag({url: 'https://cdnjs.cloudflare.com/ajax/libs/socket.io/2.1.1/socket.io.js'});
    const resultsSelector = '.a1e';
    await page.waitForSelector(resultsSelector);
    let output = await getGame(page, resultsSelector);
    await(100);

    for (let i = 0; i < 1; i++) {
        time = parseInt(output[i].time.substr(0, 2));
        if (time < 90 && time > 1) {
            output[i].more = await details(page, output[i].game_url);
        }
    }
    fs.writeFileSync("betano.json", JSON.stringify(output, null, 4));
    fs.writeFileSync("start_bet.json", JSON.stringify(output, null, 4));
/*
var my_awesome_script = document.createElement('script');
my_awesome_script.setAttribute('src','https://cdnjs.cloudflare.com/ajax/libs/socket.io/2.1.1/socket.io.js');
document.head.appendChild(my_awesome_script);
var url = "http://localhost:3000";
var socket = io(url);
socket.emit('eveniment', { my: 'data' });
 */
}
var a = 0;
async function getGame(page, resultsSelector) {
    resultsSelector = '.a1e';
    const games = JSON.parse(fs.readFileSync('betanolive.json'));
    if(parseInt(games[0]['time'].substr(0,2)) == 90){
        fs.writeFileSync("final_game_bet.json", JSON.stringify(games[0], null, 4));
    }
    console.log("getGame");
    if (a<1) {
        a ++;
        return await page.evaluate(resultsSelector => {
            const $ = window.jQuery;
            return $(resultsSelector).find('.js-evt-link.a0s').map(function () {
                return {
                    home: $(this).find('.a0t.a0u span').text(),
                    away: $(this).find('.a0t.a0v span').text(),
                    score: $(this).find('.a0k span:first-child').text() + "-" + $(this).find('.a0k span:last-child').text(),
                    time: $(this).parent().parent().find('.a0l.j span:nth-child(2)').text(),
                    game_url: $(this).attr('href'),
                    more: []
                };
            }).toArray();
        }, resultsSelector);
    } else {
        return await page.evaluate(resultsSelector => {
            const $ = window.jQuery;
            return $(resultsSelector).find('.js-evt-link.a0s').map(function () {
                return {
                    home: $(this).find('.a0t.a0u span').text(),
                    away: $(this).find('.a0t.a0v span').text(),
                    score: $(this).parent().parent().find('.a0g.dh .a0k span:first-child').text() + "-" + $(this).parent().parent().find('.a0g.dh .a0k span:last-child').text(),
                    time: $(this).parent().parent().find('.a0l.j span:nth-child(2)').text(),
                    game_url: $(this).attr('href'),
                    more: []
                };
            }).toArray();
        }, resultsSelector);
    }
}

async function betlive(page, game) {
    console.log('betlive');
    let output2 = await getGame(page, 'sad');
    fs.writeFileSync("betano.json", JSON.stringify(output2, null, 4));


    let resultsSelector = ".a0e.ar.js-market-holder.mm";
    const output = JSON.parse(fs.readFileSync('betano.json'));
    for (let i = 0; i < 1; i++) {
        if (output[i].game_url == game) {
            console.log("gasit");
            output[i].more = await odds(page, resultsSelector);
            // console.log( await odds(page, resultsSelector));
        }
    }

    fs.writeFileSync("betanolive.json", JSON.stringify(output, null, 4));

}

async function details(page, game) {

    let resultsSelector = ".a0e.ar.js-market-holder.mm";
    await page.goto(`https://ro.betano.com${game}`);
    await page.waitForSelector(resultsSelector);
    var a = 0;
    setInterval(function () {
        betlive(page, game);
        console.log(a++);
        if (a === 10) {
            clearInterval();
        }
    }, 1 * 1000);


    return await odds(page, resultsSelector)
}

async function odds(page, resultsSelector) {
    return page.evaluate(resultsSelector => {
        const $ = window.jQuery;
        return $(resultsSelector).map(function () {
            return {
                type: $(this).find('.js-market-title-text').text(),
                options: $(this).find('.a0x.w.js-selection').map(function () {
                    return {
                        bet: $(this).find('.mb.a0z').text().toString(),
                        odds: $(this).find('.ma.f.a10').text()
                    }
                }).toArray()
            }
        }).toArray();
    }, resultsSelector);
}